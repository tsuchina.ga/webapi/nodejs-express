var express = require('express');
var router = express.Router();

// DBアクセス
var pgp = require("pg-promise")();
var db = pgp("postgres://postgres:''@172.17.0.2:5432/webapi");

router.get('/', function(req, res, next) {
  db.any("SELECT * FROM users WHERE is_deleted = 0")
  .then(data => { res.send({"users": data}) })
});

router.post('/', function(req, res, next) {
  db.none("INSERT INTO users (name, age) VALUES (${name}, ${age})",{
    "name": req.body.name,
    "age": req.body.age
  })
  .then(data => { res.send({"res": "success"}) })
  .catch(error => { res.send({"res": "error"}) })
});

router.get('/:id', function(req, res, next) {
  db.one("SELECT * FROM users WHERE is_deleted = 0 AND id = $1", req.params.id)
  .then(data => { res.send(data) })
  .catch(error => { res.send({"message": "not found"}) })
});

router.put('/:id', function(req, res, next) {
  var query = "";
  query += " UPDATE users SET updated_at = CURRENT_TIMESTAMP "
  if (req.body.name != undefined) query += " , name = ${name} "
  if (req.body.age != undefined) query += " , age = ${age} "
  query += " WHERE id = ${id} "

  var params = req.body;
  params.id = req.params.id;

  db.none(query, params)
  .then(data => { res.send({"res": "success"}) })
  .catch(error => { res.send({"res": "error"}) })
});

router.delete('/:id', function(req, res, next) {
  db.none("UPDATE users SET is_deleted = 1, updated_at = CURRENT_TIMESTAMP WHERE id = $1", req.params.id)
  .then(data => { res.send({"res": "success"}) })
  .catch(error => { res.send({"res": "error"}) })
});

module.exports = router;
