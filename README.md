# nodejs + ExpressでつくるRESTful API

## 参考

* [Express - Node.js Web アプリケーション・フレームワーク](http://expressjs.com/ja/)

## 環境構築

これまで通り開発はローカル環境で、テストや実行環境はDockerで用意します。

ローカルはWindows10 64bit ProでHyper-Vを使ってDockerを動かしています。


0. Imageのダウンロード

  ```
  > docker pull node:9.8
  ```

0. Containerの作成

  ```
  > docker run -d -it --name nodejs-express -p 3000:3000 -v E:/Projects/tsuchinaga/webapi/nodejs-express:/root -w /root node:9.8
  ```

0. アクセスして確認

  ```
  > docker exec -it nodejs-express bash
  $ node -v
  v9.8.0
  $ npm -v
  5.6.0
  ```

0. Express環境

  ```
  $ npm install -g express-generator
  + express-generator@4.16.0
  added 10 packages and removed 49 packages in 2.344s
  npm info ok

  $ express --view=pug myapp
  $ mv myapp/* ./
  $ rm -d myapp/
  $ npm install
  added 31 packages and removed 21 packages in 7.767s
  $ DEBUG=myapp:* npm start
  ```

  [localhost:3000](http://localhost:3000)でアクセスするとWelcomeがみれる


## ルーティング

`app.js`にルーティングを記載することになる。
`app.js`で処理を行なっているjsファイルをrequireし、app.useでパスと紐づける。

処理を行なっているjsファイルrouterに諸々の処理を物故んでいき、最後にexportすることになる。


仕組みとしては分かりやすいけど、使いやすいかは別問題かなぁ


* app.js(抜粋)

  ```javascript
  var usersRouter = require('./routes/v1/users');
  app.use('/v1/users', usersRouter);
  ```

* routes/v1/users.js

  ```javascript
  var express = require('express');
  var router = express.Router();

  /* GET users listing. */
  router.get('/', function(req, res, next) {
    res.send('respond with a resource');
  });

  /* GET users/:id listing. */
  router.get('/:id', function(req, res, next) {
    res.send('get users id: ' + req.params.id);
  });

  module.exports = router;
  ```


## DBアクセス

他と同じくPostgreSQLサーバにアクセスしてデータを取ってくる

0. pluginの追加

  ```
  $ npm install -s pg-promise
  + pg-promise@8.2.2
  added 20 packages in 5.068s
  ```

0. connection設定

  ```javascript
  // DBアクセス
  var pgp = require("pg-promise")();
  var db = pgp("postgres://postgres:''@172.17.0.2:5432/webapi");

  db.any("SELECT * FROM users WHERE is_deleted = 0")
  .then(function (data) {
    res.send({"users": data});
  })
  ```

めっちゃくちゃ簡単！！


## 感想

まだまだ使いこなせているわけではないけど、pg-promiseはちょっと緩すぎるのでは？といった感じ。
DB周りはもうちょっとかっちりしたのが使いたいかなー。

リクエストを受けてレスポンスを返すまではすごく直感的。

実務に耐えうるのを作れる気はまだしてないけど、気軽に作れるという点ではすごくいい。
